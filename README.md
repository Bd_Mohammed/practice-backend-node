# 🧰 Simple TypeScript Starter | 2022

> We talk about a lot of **advanced Node.js and TypeScript** concepts on [the blog](https://khalilstemmler.com), particularly focused around Domain-Driven Design and large-scale enterprise application patterns. However, I received a few emails from readers that were interested in seeing what a basic TypeScript starter project looks like. So I've put together just that.

### Features

- Minimal
- TypeScript v4
- Testing with Jest
- Linting with Eslint and Prettier
- Pre-commit hooks with Husky
- VS Code debugger scripts
- Local development with Nodemon

### Scripts

#### `yarn run start:dev`

Starts the application in development using `nodemon` and `ts-node` to do hot reloading.

#### `yarn run start`

Starts the app in production by first building the project with `yarn run build`, and then executing the compiled JavaScript at `build/index.js`.

#### `yarn run build`

Builds the app at `build`, cleaning the folder first.

#### `yarn run test`

Runs the `jest` tests once.

#### `yarn run test:dev`

Run the `jest` tests in watch mode, waiting for file changes.

#### `yarn run prettier-format`

Format your code.

#### `yarn run prettier-watch`

Format your code in watch mode, waiting for file changes.


backend
│
└─── dist                   #all the javascript files are here
└─── lib                    #all the typescript files are here
      └─── config           #app configurations files
            └─── app.ts     #app starting point            
      └─── controllers      #request managers
      └─── modules          #schemas, interfaces, services
      └─── routes           #define the endpoints
      └─── environment.ts   #store all environment variables
      └─── server.js        #HTTP server that listens to server port
└─── .gitignore             #git ignore file
└─── yarn.json              #yarn automatically generated document
└─── package.json           #holds metadata and yarn packagage list
└─── tsconfig.json          #specify the root level files and the compiler options