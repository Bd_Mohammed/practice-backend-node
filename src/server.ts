import { LoggerService } from './lib/logger/logger';
import app from "./lib/config/app";
import env from './enviorment';

const port = env.getPort();
const server_logger: LoggerService =  new LoggerService('Server');

app.listen(port, () => {
   server_logger.info(`[server]: Server is running at http://localhost:${port}`);
})