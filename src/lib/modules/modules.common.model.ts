import { Response } from 'express';
import { LoggerService } from '../logger/logger';
import { response_status_codes } from './modules.service.model';

export function
    successResponse(message: string, DATA: any, res: Response, logger: LoggerService) {
    logger.info(`[Response] status code : ${response_status_codes.success}`, {
        STATUS: 'SUCCESS',
        MESSAGE: message,
        DATA
    });
    res.status(response_status_codes.success).json({
        STATUS: 'SUCCESS',
        MESSAGE: message,
        DATA
    });
}

export function failureResponse(message: string, DATA: any, res: Response, logger: LoggerService) {
    logger.error(`[Response] status code : ${response_status_codes.bad_request}`, {
        STATUS: 'FAILURE',
        MESSAGE: message,
        DATA
    });
    res.status(response_status_codes.bad_request).json({
        STATUS: 'FAILURE',
        MESSAGE: message,
        DATA
    });
}

export function insufficientParameters(res: Response, data: any, logger: LoggerService) {
    logger.info(`[Response] status code : ${response_status_codes.bad_request}`, {
        STATUS: 'FAILURE',
        MESSAGE: 'Insufficient parameters',
        DATA: data,
    });
    res.status(response_status_codes.bad_request).json({
        STATUS: 'FAILURE',
        MESSAGE: 'Insufficient parameters',
        DATA: data,
    });
}

export function mongoError(err: any, res: Response, logger: LoggerService) {
    logger.error(`[Response] status code : ${response_status_codes.internal_server_error}`, {
        STATUS: 'FAILURE',
        MESSAGE: 'MongoDB error',
        DATA: err
    });
    res.status(response_status_codes.internal_server_error).json({
        STATUS: 'FAILURE',
        MESSAGE: 'MongoDB error',
        DATA: err
    });
}