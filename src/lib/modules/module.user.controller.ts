import { IUserAuth } from './modules.user.schema';
import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from './modules.common.model';
import UserAuthService from './modules.user.service';
import { LoggerService } from '../logger/logger';
import { generateToken } from '../Jwt/jwt.utils';
import { parameters, processRequest } from '../customYup/customYup';

export class UserAuthController {

    private user_auth_service: UserAuthService = new UserAuthService();

    public create_user(req: Request, res: Response, logger: LoggerService, user_custom_yup: parameters) {
        // this check whether all the fields were send through the request or not
        logger.info('[Request]: create_user param', req.body);

        try {
            const processReq = processRequest(user_custom_yup, req.body, 'post');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                insufficientParameters(res, processReq, logger);
                return;
            }
        } catch (error) {
            logger.error(`[ERROR]: create_user process request`, error);
            failureResponse('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }
        const user_params: IUserAuth = {
            name: req.body.name.trim(),
            email: req.body.email.trim(),
            role: req.body.role,
            password: req.body.password.trim(),
            modification_notes: [{
                modified_on: new Date(Date.now()),
                modified_by: null,
                modification_note: 'New user created'
            }],

        };
        const user_filter = { email: req.body.email };
        this.user_auth_service.filterUser(user_filter, (err: any, user_data: IUserAuth) => {
            if (err) {
                mongoError(err, res, logger);
            } else {
                if (!user_data) {
                    this.user_auth_service.createUser(user_params, (err: any, user_data: IUserAuth | any) => {
                        if (err) {
                            mongoError(err, res, logger);
                        } else {
                            try {
                                const response = {
                                    token: generateToken(user_params),
                                    ...user_data._doc,
                                };
                                successResponse('create user successful', response, res, logger);
                            } catch (error) {
                                logger.error('[Error] in sending response : ', error);
                                failureResponse('Internal Server Error', {
                                    message: 'Internal Server Error'
                                }, res, logger);
                            }
                        }
                    });
                } else {
                    failureResponse('User Already Exists', {
                        message: 'User Already Exists'
                    }, res, logger);
                }
            }
        });
    }

    public validate_user(req: Request, res: Response, logger: LoggerService, user_custom_yup: parameters) {
        logger.info('[Request]: get_user param', req.params);
        try {
            const processReq = processRequest(user_custom_yup, req.body, 'get');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                insufficientParameters(res, processReq, logger);
                return;
            }
        } catch (error) {
            logger.error(`[ERROR]: get_user process request`, error);
            failureResponse('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }
        const user_filter = { email: req.body.email };
        this.user_auth_service.filterUser(user_filter, (err: any, user_data: IUserAuth | any): void => {
            if (err) {
                mongoError(err, res, logger);
            } else {
                if (user_data) {
                    if(user_data.password === req.body.password){
                        try {
                            const response = {
                                token: generateToken(user_data),
                                ...user_data._doc,
                            };
                            successResponse('GET User Successful', response, res, logger);
                        } catch (error) {
                            logger.error('[Error] in sending response : ', error);
                            failureResponse('Internal Server Error', {
                                message: 'Internal Server Error'
                            }, res, logger);
                        }
                    }else{
                        failureResponse('Incorrect password', {
                            message: 'Incorrect password'
                        }, res, logger);
                    }
                } else {
                    failureResponse('User Not Found', {
                        message: 'User Not Found'
                    }, res, logger);
                }
            }
        });
    }

    public update_user(req: Request, res: Response, logger: LoggerService, user_custom_yup: parameters) {
        logger.info('[Request]: update_user param', {
            body: req.body,
            params: req.params
        });
        try {
            const processReq = processRequest(user_custom_yup, {
                id: req.params.id,
                ...req.body
            }, 'update');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                insufficientParameters(res, processReq, logger);
                return;
            }
        } catch (error) {
            logger.error(`[ERROR]: update_user process request`, error);
            failureResponse('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }


        const user_filter = { _id: req.params.id };
        this.user_auth_service.filterUser(user_filter, (err: any, user_data: IUserAuth) => {
            if (err) {
                mongoError(err, res, logger);
            } else if (user_data) {
                user_data.modification_notes.push({
                    modified_on: new Date(Date.now()),
                    modified_by: null,
                    modification_note: 'User data updated'
                });
                const user_params: IUserAuth = {
                    _id: req.params.id,
                    name: req.body.name ? req.body.name : user_data.name,
                    email: req.body.email ? req.body.email : user_data.email,
                    role: req.body.role ? req.body.role : user_data.role,
                    password: req.body.password ? req.body.password : user_data.password,
                    is_deleted: req.body.is_deleted ? req.body.is_deleted : user_data.is_deleted,
                    modification_notes: user_data.modification_notes
                };
                this.user_auth_service.updateUser(user_params, (err: any, user_data: IUserAuth) => {
                    if (err) {
                        mongoError(err, res, logger);
                    } else {
                        successResponse('update user successful', user_data, res, logger);
                    }
                });
            } else {
                failureResponse('Invalid User', {
                    message: 'Invalid User'
                }, res, logger);
            }
        });
    }

    public delete_user(req: Request, res: Response, logger: LoggerService, user_custom_yup: parameters) {
        logger.info('[Request]: delete_user param', req.params);
        try {
            const processReq = processRequest(user_custom_yup, req.params, 'delete');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                insufficientParameters(res, processReq, logger);
                return;
            }
        } catch (error) {
            logger.error(`[ERROR]: delete_user process request`, error);
            failureResponse('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }
        // if (req.params.id) {
        this.user_auth_service.deleteUser(req.params.id, (err: any, delete_details: any) => {
            if (err) {
                mongoError(err, res, logger);
            } else if (delete_details.deletedCount !== 0) {
                successResponse('User Deleted Successful', {
                    message: 'User Deleted Successful'
                }, res, logger);
            } else {
                failureResponse('Invalid User', {
                    message: 'Invalid User'
                }, res, logger);
            }
        });
    }
}