import { ModificationNote } from './modules.service.model';

export interface IUserAuth {
    _id?: String;
    name: String;
    email: String;
    role: String;
    password: String;
    is_deleted?: Boolean;
    modification_notes: ModificationNote[];
}
