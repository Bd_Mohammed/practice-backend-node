import { mixed, num, rEmail, rUrl, rUUID, str } from "./Constants";
import { IMethodsSchema, ISchemaOption, ITypeSchemaOption } from "./ICustomYup";

class parameters {
    private fields: object = {};
    private nodes: string[] = [];
    constructor(spec: object) {
        this.fields = Object.assign({}, spec);
        this.nodes = Object.keys(spec);
    }
}

class Schema {
    private type: any;
    private _typeCheck: (value: any) => boolean;
    private typeMessage: string;
    private requestMethod: IMethodsSchema;
    private tests: any[];

    constructor(options: ISchemaOption) {
        this.type = options.type;
        this._typeCheck = options.check;
        this.typeMessage = options.message;
        this.requestMethod = options.requestMethod;
        this.tests = [];
    }
    clone() {
        const next = Object.create(Object.getPrototypeOf(this));
        next.type = this.type;
        next._typeCheck = this._typeCheck;
        next.typeMessage = this.typeMessage;
        next.tests = [...this.tests];
        next.requestMethod = this.requestMethod;
        return next;
    }
    test(...args: any) {
        let opts;
        if (args.length === 1) {
            if (typeof args[0] === 'function') {
                opts = {
                    test: args[0]
                };
            } else {
                opts = args[0];
            }
        }
        if (opts.message === undefined) opts.message = mixed.default;
        if (typeof opts.test !== 'function') throw new TypeError('`test` is a required parameters');
        let next = this.clone();
        next.tests.push(...args);
        return next;
    }
}

class StringSchema extends Schema {
    private _options: any;
    constructor(option: ITypeSchemaOption) {
        super({
            type: 'string',
            message: str.type,
            check(value: any) {
                if (value instanceof String) value = value.valueOf();
                return typeof value === 'string';
            },
            requestMethod: option.methodsSchema,
        });
        this._options = [];
    }
    min(min: number, message = str.min) {
        return this.test({
            message,
            name: 'min',
            params: {
                min
            },
            test(value: string) {
                return value.trim().length >= min;
            }
        });
    }
    max(max: number, message = str.max) {
        return this.test({
            message,
            name: 'max',
            params: {
                max
            },
            test(value: string) {
                return value.trim().length <= max;
            }
        });
    }
    matches(regex: any, options: any) {
        let excludeEmptyString = false;
        let message;
        let name;
        if (options) {
            if (typeof options === 'object') {
                ({
                    excludeEmptyString = false,
                    message,
                    name
                } = options);
            } else {
                message = options;
            }
        }
        return this.test({
            name: name || 'matches',
            message: message || str.matches,
            params: {
                regex
            },
            skipAbsent: true,
            test: (value: string) => value.trim() === '' && excludeEmptyString || value.trim().search(regex) !== -1
        });
    }
    email(message = str.email) {
        return this.matches(rEmail, {
            name: 'email',
            message,
            excludeEmptyString: true
        });
    }
    url(message = str.url) {
        return this.matches(rUrl, {
            name: 'url',
            message,
            excludeEmptyString: true
        });
    }
    uuid(message = str.uuid) {
        return this.matches(rUUID, {
            name: 'uuid',
            message,
            excludeEmptyString: false
        });
    }
    // required(message = str.required) {
    //     return this.test({
    //         message: message,
    //         name: 'required',
    //         test(value) {
    //             return value.length !== 0;
    //         }
    //     });
    // }
}
class NumberSchema extends Schema {
    private _options: any;
    constructor(option: ITypeSchemaOption) {
        super({
            type: 'number',
            message: num.type,
            check(value: any) {
                if (value instanceof Number) value = value.valueOf();
                return typeof value === 'number';
            },
            requestMethod: option.methodsSchema,
        });
        this._options = [];
    }
    min(min: number, message = num.min) {
        return this.test({
            message,
            name: 'min',
            params: {
                min
            },
            test(value: number) {
                return value >= min;
            }
        });
    }
    max(max: number, message = num.max) {
        return this.test({
            message,
            name: 'max',
            params: {
                max
            },
            test(value: number) {
                return value <= max;
            }
        });
    }
    oneOf(oneOf: number[], message: string) {
        return this.test({
            message,
            name: 'oneOf',
            params: {
                oneOf
            },
            test(value: number) {
                return !!oneOf.find(e => e === value);
            }
        });
    }
}

const checkForRequiredParam = (SchemaObjectFields: any, property: string, validationObject: any, _obj: any) => {
    let testLength = SchemaObjectFields[property].tests.length;
    for (let index = 0; index < testLength; index++) {
        let tester = SchemaObjectFields[property].tests[index];
        // console.log(tester);
        if (!tester.test(validationObject[property])) {
            _obj[`${property}`] = tester.message.replace('${path}', property).replace('${' + `${tester.name}` + '}', tester.params[tester.name]);
            break;
        }
    }
}

const validationSchema = (obj: any): parameters => {
    return new parameters(obj);
}

const string = (options: ITypeSchemaOption) => {
    return new StringSchema(options);
}

const number = (options: ITypeSchemaOption) => {
    return new NumberSchema(options);
}

const processRequest = (SchemaObject: any, validationObject: any, method: any) => {
    // console.log(JSON.stringify(validationObject));
    // console.log(JSON.stringify(SchemaObject));
    // console.log(Object.keys(SchemaObject.fields).length);
    // console.log(Object.keys(validationObject).length);


    if (Object.keys(SchemaObject.fields).length >= Object.keys(validationObject).length) {
        let _obj: any = {};
        let SchemaObjectFields = SchemaObject.fields;
        for (const property in SchemaObjectFields) {
            // console.log('property',property);
            // console.log('SchemaObjectFields[property]',SchemaObjectFields[property]);
            if (SchemaObjectFields[property].requestMethod[method]) {
                if (validationObject[property]) {
                    // if (typeof validationObject[property] === SchemaObjectFields[property].type) {
                        if (SchemaObjectFields[property]._typeCheck(validationObject[property])) {
                            // console.log('SchemaObjectFields[property].tests',SchemaObjectFields[property].tests);
                            checkForRequiredParam(SchemaObjectFields, property, validationObject, _obj);
                        } else {
                            _obj[`${property}`] = SchemaObjectFields[property].typeMessage.replace('${path}', property);
                        }
                    // }
                } else {
                    _obj[`${property}`] = `${property} field is mandatory`
                }
            } else {
                if (validationObject[property]) {
                    if (SchemaObjectFields[property]._typeCheck(validationObject[property])) {
                        // console.log('SchemaObjectFields[property].tests',SchemaObjectFields[property].tests);
                        checkForRequiredParam(SchemaObjectFields, property, validationObject, _obj);
                    } else {
                        _obj[`${property}`] = SchemaObjectFields[property].typeMessage.replace('${path}', property);
                    }
                }
            }
        }
        // console.log(JSON.stringify(_obj));
        return _obj;
    } else {
        throw {
            message: 'No of keys In Schema Object and valodation object must be same',
        }
    }
}

export {
    string,
    validationSchema,
    parameters,
    processRequest,
    number,
}