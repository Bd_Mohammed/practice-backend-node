interface IMethodsSchema {
    post?: boolean;
    get?: boolean;
    delete?: boolean;
    patch?: boolean;
    update?: boolean;

}
interface ITypeSchemaOption {
    methodsSchema: IMethodsSchema
}

interface ISchemaOption {
    type: any;
    message: string;
    check(value: any): boolean;
    requestMethod: IMethodsSchema;
}


export {
    ITypeSchemaOption,
    ISchemaOption,
    IMethodsSchema,
}