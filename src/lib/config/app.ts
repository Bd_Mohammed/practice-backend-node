// import { TestRoutes } from './../routes/test_routes';
import express from 'express';
import bodyParser from 'body-parser';
import enviorment from '../../enviorment';
import mongoose from 'mongoose';
import { UserRoutes } from '../routes/user_routes';
import { LoggerService } from '../logger/logger';
// import { TestRoutes } from '../routes/test_routes';
import cors from 'cors';
class App {
  public app: express.Application;
  private app_logger: LoggerService = new LoggerService('App')
  public mongoUrl: string = 'mongodb://127.0.0.1:27017/' + enviorment.getDBName();
  public router = express.Router();
  // private test_routes: TestRoutes = new TestRoutes();

  private user_route: UserRoutes = new UserRoutes(express);


  constructor() {
    this.app = express();
    this.app.use(cors())
    this.config();
    this.mongoSetup();
    this.app.use('/api/auth',this.user_route.router);
  }
  private config(): void {
    // support application/json type post data
    this.app.use(bodyParser.json());
    //support application/x-www-form-urlencoded post data
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }
  private mongoSetup(): void {
    this.app_logger.info(this.mongoUrl);
    mongoose.connect(this.mongoUrl);
  }
}
export default new App().app;
