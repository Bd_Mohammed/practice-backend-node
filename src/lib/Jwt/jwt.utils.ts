import { IUserAuth } from './../modules/modules.user.schema';
import { sign, SignOptions } from 'jsonwebtoken';
/**
 * generates JWT used for local testing
 */
export const generateToken = (payload: IUserAuth) => {
    const privateKey = `secret`;
    const signInOptions: SignOptions = {
        // RS256 uses a public/private key pair. The API provides the private key 
        // to generate the JWT. The client gets a public key to validate the 
        // signature
        // algorithm: 'RS256',
        expiresIn: '24h'
    };
    
    // generate JWT
    return sign({
        name: payload.name,
        email: payload.email,
        role: payload.role,
        password: payload.password
      }, `urethndvkngkjdbgkdkdnbdmbmdbdf`, signInOptions);
};