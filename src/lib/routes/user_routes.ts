import { Request, Response, Router } from 'express';
import { number, parameters, string, validationSchema } from '../customYup/customYup';
import { LoggerService } from '../logger/logger';
import { UserAuthController } from '../modules/module.user.controller';

export class UserRoutes {

    private user_controller: UserAuthController = new UserAuthController();
    private user_logger: LoggerService = new LoggerService('User');
    public router: Router;
    private user_custom_yup: parameters = validationSchema({
        id:string({
            methodsSchema: {
                post: false,
                get: false,
                // delete: true,
                // update: true,
                // patch: true,
            }
        }).uuid("please enter valid Id"),
        name: string({
            methodsSchema: {
                post: true,
                get: false,
                // delete: false,
                // update: true,
                // patch: true,
            }
        }).min(3).max(10),
        email: string({
            methodsSchema: {
                post: true,
                get: true,
                // delete: false,
                // update: false,
                // patch: false,
            }
        }).email("please enter valid email"),
        password: string({
            methodsSchema: {
                post: true,
                get: true,
                // delete: false,
                // update: false,
                // patch: false,
            }
        })
        .min(8, "Password must be 8 characters long")
        .matches(/[0-9]/, "Password requires a number")
        .matches(/[a-z]/, "Password requires a lowercase letter")
        .matches(/[A-Z]/, "Password requires an uppercase letter")
        .matches(/[^\w]/, "Password requires a symbol"),
        role: number({
            methodsSchema: {
                post: true,
                get: false,
                // delete: false,
                // update: false,
                // patch: false,
            }
        }).oneOf([1,2],'role must be a 1 or 2'),
    });

    constructor(express: any){
        this.router = express.Router();
        this.route();
    }
    public route(): void {

        //create a new user
        this.router.post('/signup', (req: Request, res: Response) => {
            this.user_controller.create_user(req, res, this.user_logger, this.user_custom_yup);
        });

        
        this.router.post('/signin', (req: Request, res: Response) => {
            this.user_controller.validate_user(req, res, this.user_logger, this.user_custom_yup);
        });

        // //get an existing user
        // this.router.get('/user/:id', (req: Request, res: Response) => {
        //     this.user_controller.get_user(req, res, this.user_logger, this.user_custom_yup);
        // });

        // this.router.put('/user/:id', (req: Request, res: Response) => {
        //     this.user_controller.update_user(req, res, this.user_logger, this.user_custom_yup);
        // });

        // this.router.delete('/user/:id', (req: Request, res: Response) => {
        //     this.user_controller.delete_user(req, res, this.user_logger, this.user_custom_yup);
        // });

        // Mismatch URL
        this.router.all('*', (req: Request, res: Response) => {
            this.user_logger.info('Check your URL please');
            res.status(404).send({ error: true, message: 'Check your URL please' });
        });
    }
}
