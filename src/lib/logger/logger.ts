import winston, { createLogger, format, transports, config } from 'winston';
class LoggerService {
  log_data: any;
  route: String;
  logger: winston.Logger;
  
  constructor(route: String) {
    this.log_data = null;
    this.route = route;
    const logger = winston.createLogger({
      levels: config.syslog.levels,
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({
          filename: `./logs/${this.route}.log`
        }),
      ],
      format: winston.format.printf((info) => {
        let message = `[${this.dateFormat()}][${info.level.toUpperCase()}][${this.route}.log] : ${info.message}`
        message = info.obj ? message + ` | data:${JSON.stringify(info.obj)}` : message;
        message = this.log_data ? message + `log_data:${JSON.stringify(this.log_data)}` : message
        return message
      })
    });
    this.logger = logger
  }

  dateFormat = (): string => {
    return new Date(Date.now()).toJSON();
  }

  setLogData(log_data: string) {
    this.log_data = log_data
  }

  async info(message: string, obj?: any): Promise<void> {
    if(obj){
      this.logger.log('info', message, {
        obj
      })
    }else{
      this.logger.log('info', message);
    }
  }

  async debug(message: string, obj?: any): Promise<void> {
    if(obj){
      this.logger.log('debug', message, {
        obj
      })
    }else{
      this.logger.log('debug', message);
    }
  }

  async error(message: string, obj?: any): Promise<void> {
    if(obj){
      this.logger.log('error', message, {
        obj
      })
    }else{
      this.logger.log('error', message);
    }
  }

  
  // async debug(message: string) {
  //   this.logger.log('debug', message);
  // }
  // async debug(message: string, obj: any) {
  //   this.logger.log('debug', message, {
  //     obj
  //   })
  // }
  // async error(message: string) {
  //   this.logger.log('error', message);
  // }
  // async error(message: string, obj: any) {
  //   this.logger.log('error', message, {
  //     obj
  //   })
  // }
}

//{"message":"New user created.","level":"info"}


const paymentLogger = createLogger({
  transports: [
    new transports.Console()
  ]
});


// // require logger.js
// const {userLogger, paymentLogger} = require('./logger');

// userLogger.info('New user created');
export {
  LoggerService,
  paymentLogger
};

