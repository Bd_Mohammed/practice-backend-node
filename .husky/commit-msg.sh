#!/bin/bash

commit_msg_file=$1
commit_msg=$(cat "$commit_msg_file")

# Add your regex pattern here
regex_pattern="^(https?:\/\/)?(www\.)?trello\.com\/[a-zA-Z0-9_-]+\/[a-zA-Z0-9_-]+\/[a-zA-Z0-9_-]+$"
if ! [[ $commit_msg =~ $regex_pattern ]]; then
  echo "Invalid commit message. Please follow the specified format."
  exit 1
fi
