"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("./lib/logger/logger");
var app_1 = __importDefault(require("./lib/config/app"));
var enviorment_1 = __importDefault(require("./enviorment"));
var port = enviorment_1.default.getPort();
var server_logger = new logger_1.LoggerService('Server');
app_1.default.listen(port, function () {
    server_logger.info("[server]: Server is running at http://localhost:".concat(port));
});
