"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateToken = void 0;
var jsonwebtoken_1 = require("jsonwebtoken");
/**
 * generates JWT used for local testing
 */
var generateToken = function (payload) {
    var privateKey = "secret";
    var signInOptions = {
        // RS256 uses a public/private key pair. The API provides the private key 
        // to generate the JWT. The client gets a public key to validate the 
        // signature
        // algorithm: 'RS256',
        expiresIn: '24h'
    };
    // generate JWT
    return (0, jsonwebtoken_1.sign)({
        name: payload.name,
        email: payload.email,
        role: payload.role,
        password: payload.password
    }, "urethndvkngkjdbgkdkdnbdmbmdbdf", signInOptions);
};
exports.generateToken = generateToken;
