"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { TestRoutes } from './../routes/test_routes';
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var enviorment_1 = __importDefault(require("../../enviorment"));
var mongoose_1 = __importDefault(require("mongoose"));
var user_routes_1 = require("../routes/user_routes");
var logger_1 = require("../logger/logger");
// import { TestRoutes } from '../routes/test_routes';
var cors_1 = __importDefault(require("cors"));
var App = /** @class */ (function () {
    function App() {
        this.app_logger = new logger_1.LoggerService('App');
        this.mongoUrl = 'mongodb://127.0.0.1:27017/' + enviorment_1.default.getDBName();
        this.router = express_1.default.Router();
        // private test_routes: TestRoutes = new TestRoutes();
        this.user_route = new user_routes_1.UserRoutes(express_1.default);
        this.app = (0, express_1.default)();
        this.app.use((0, cors_1.default)());
        this.config();
        this.mongoSetup();
        this.app.use('/api/auth', this.user_route.router);
    }
    App.prototype.config = function () {
        // support application/json type post data
        this.app.use(body_parser_1.default.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(body_parser_1.default.urlencoded({ extended: false }));
    };
    App.prototype.mongoSetup = function () {
        this.app_logger.info(this.mongoUrl);
        mongoose_1.default.connect(this.mongoUrl);
    };
    return App;
}());
exports.default = new App().app;
