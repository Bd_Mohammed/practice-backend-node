"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoutes = void 0;
var customYup_1 = require("../customYup/customYup");
var logger_1 = require("../logger/logger");
var module_user_controller_1 = require("../modules/module.user.controller");
var UserRoutes = /** @class */ (function () {
    function UserRoutes(express) {
        this.user_controller = new module_user_controller_1.UserAuthController();
        this.user_logger = new logger_1.LoggerService('User');
        this.user_custom_yup = (0, customYup_1.validationSchema)({
            id: (0, customYup_1.string)({
                methodsSchema: {
                    post: false,
                    get: false,
                    // delete: true,
                    // update: true,
                    // patch: true,
                }
            }).uuid("please enter valid Id"),
            name: (0, customYup_1.string)({
                methodsSchema: {
                    post: true,
                    get: false,
                    // delete: false,
                    // update: true,
                    // patch: true,
                }
            }).min(3).max(10),
            email: (0, customYup_1.string)({
                methodsSchema: {
                    post: true,
                    get: true,
                    // delete: false,
                    // update: false,
                    // patch: false,
                }
            }).email("please enter valid email"),
            password: (0, customYup_1.string)({
                methodsSchema: {
                    post: true,
                    get: true,
                    // delete: false,
                    // update: false,
                    // patch: false,
                }
            })
                .min(8, "Password must be 8 characters long")
                .matches(/[0-9]/, "Password requires a number")
                .matches(/[a-z]/, "Password requires a lowercase letter")
                .matches(/[A-Z]/, "Password requires an uppercase letter")
                .matches(/[^\w]/, "Password requires a symbol"),
            role: (0, customYup_1.number)({
                methodsSchema: {
                    post: true,
                    get: false,
                    // delete: false,
                    // update: false,
                    // patch: false,
                }
            }).oneOf([1, 2], 'role must be a 1 or 2'),
        });
        this.router = express.Router();
        this.route();
    }
    UserRoutes.prototype.route = function () {
        var _this = this;
        //create a new user
        this.router.post('/signup', function (req, res) {
            _this.user_controller.create_user(req, res, _this.user_logger, _this.user_custom_yup);
        });
        this.router.post('/signin', function (req, res) {
            _this.user_controller.validate_user(req, res, _this.user_logger, _this.user_custom_yup);
        });
        // //get an existing user
        // this.router.get('/user/:id', (req: Request, res: Response) => {
        //     this.user_controller.get_user(req, res, this.user_logger, this.user_custom_yup);
        // });
        // this.router.put('/user/:id', (req: Request, res: Response) => {
        //     this.user_controller.update_user(req, res, this.user_logger, this.user_custom_yup);
        // });
        // this.router.delete('/user/:id', (req: Request, res: Response) => {
        //     this.user_controller.delete_user(req, res, this.user_logger, this.user_custom_yup);
        // });
        // Mismatch URL
        this.router.all('*', function (req, res) {
            _this.user_logger.info('Check your URL please');
            res.status(404).send({ error: true, message: 'Check your URL please' });
        });
    };
    return UserRoutes;
}());
exports.UserRoutes = UserRoutes;
