"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.number = exports.processRequest = exports.parameters = exports.validationSchema = exports.string = void 0;
var Constants_1 = require("./Constants");
var parameters = /** @class */ (function () {
    function parameters(spec) {
        this.fields = {};
        this.nodes = [];
        this.fields = Object.assign({}, spec);
        this.nodes = Object.keys(spec);
    }
    return parameters;
}());
exports.parameters = parameters;
var Schema = /** @class */ (function () {
    function Schema(options) {
        this.type = options.type;
        this._typeCheck = options.check;
        this.typeMessage = options.message;
        this.requestMethod = options.requestMethod;
        this.tests = [];
    }
    Schema.prototype.clone = function () {
        var next = Object.create(Object.getPrototypeOf(this));
        next.type = this.type;
        next._typeCheck = this._typeCheck;
        next.typeMessage = this.typeMessage;
        next.tests = __spreadArray([], this.tests, true);
        next.requestMethod = this.requestMethod;
        return next;
    };
    Schema.prototype.test = function () {
        var _a;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var opts;
        if (args.length === 1) {
            if (typeof args[0] === 'function') {
                opts = {
                    test: args[0]
                };
            }
            else {
                opts = args[0];
            }
        }
        if (opts.message === undefined)
            opts.message = Constants_1.mixed.default;
        if (typeof opts.test !== 'function')
            throw new TypeError('`test` is a required parameters');
        var next = this.clone();
        (_a = next.tests).push.apply(_a, args);
        return next;
    };
    return Schema;
}());
var StringSchema = /** @class */ (function (_super) {
    __extends(StringSchema, _super);
    function StringSchema(option) {
        var _this = _super.call(this, {
            type: 'string',
            message: Constants_1.str.type,
            check: function (value) {
                if (value instanceof String)
                    value = value.valueOf();
                return typeof value === 'string';
            },
            requestMethod: option.methodsSchema,
        }) || this;
        _this._options = [];
        return _this;
    }
    StringSchema.prototype.min = function (min, message) {
        if (message === void 0) { message = Constants_1.str.min; }
        return this.test({
            message: message,
            name: 'min',
            params: {
                min: min
            },
            test: function (value) {
                return value.trim().length >= min;
            }
        });
    };
    StringSchema.prototype.max = function (max, message) {
        if (message === void 0) { message = Constants_1.str.max; }
        return this.test({
            message: message,
            name: 'max',
            params: {
                max: max
            },
            test: function (value) {
                return value.trim().length <= max;
            }
        });
    };
    StringSchema.prototype.matches = function (regex, options) {
        var _a;
        var excludeEmptyString = false;
        var message;
        var name;
        if (options) {
            if (typeof options === 'object') {
                (_a = options.excludeEmptyString, excludeEmptyString = _a === void 0 ? false : _a, message = options.message, name = options.name);
            }
            else {
                message = options;
            }
        }
        return this.test({
            name: name || 'matches',
            message: message || Constants_1.str.matches,
            params: {
                regex: regex
            },
            skipAbsent: true,
            test: function (value) { return value.trim() === '' && excludeEmptyString || value.trim().search(regex) !== -1; }
        });
    };
    StringSchema.prototype.email = function (message) {
        if (message === void 0) { message = Constants_1.str.email; }
        return this.matches(Constants_1.rEmail, {
            name: 'email',
            message: message,
            excludeEmptyString: true
        });
    };
    StringSchema.prototype.url = function (message) {
        if (message === void 0) { message = Constants_1.str.url; }
        return this.matches(Constants_1.rUrl, {
            name: 'url',
            message: message,
            excludeEmptyString: true
        });
    };
    StringSchema.prototype.uuid = function (message) {
        if (message === void 0) { message = Constants_1.str.uuid; }
        return this.matches(Constants_1.rUUID, {
            name: 'uuid',
            message: message,
            excludeEmptyString: false
        });
    };
    return StringSchema;
}(Schema));
var NumberSchema = /** @class */ (function (_super) {
    __extends(NumberSchema, _super);
    function NumberSchema(option) {
        var _this = _super.call(this, {
            type: 'number',
            message: Constants_1.num.type,
            check: function (value) {
                if (value instanceof Number)
                    value = value.valueOf();
                return typeof value === 'number';
            },
            requestMethod: option.methodsSchema,
        }) || this;
        _this._options = [];
        return _this;
    }
    NumberSchema.prototype.min = function (min, message) {
        if (message === void 0) { message = Constants_1.num.min; }
        return this.test({
            message: message,
            name: 'min',
            params: {
                min: min
            },
            test: function (value) {
                return value >= min;
            }
        });
    };
    NumberSchema.prototype.max = function (max, message) {
        if (message === void 0) { message = Constants_1.num.max; }
        return this.test({
            message: message,
            name: 'max',
            params: {
                max: max
            },
            test: function (value) {
                return value <= max;
            }
        });
    };
    NumberSchema.prototype.oneOf = function (oneOf, message) {
        return this.test({
            message: message,
            name: 'oneOf',
            params: {
                oneOf: oneOf
            },
            test: function (value) {
                return !!oneOf.find(function (e) { return e === value; });
            }
        });
    };
    return NumberSchema;
}(Schema));
var checkForRequiredParam = function (SchemaObjectFields, property, validationObject, _obj) {
    var testLength = SchemaObjectFields[property].tests.length;
    for (var index = 0; index < testLength; index++) {
        var tester = SchemaObjectFields[property].tests[index];
        // console.log(tester);
        if (!tester.test(validationObject[property])) {
            _obj["".concat(property)] = tester.message.replace('${path}', property).replace('${' + "".concat(tester.name) + '}', tester.params[tester.name]);
            break;
        }
    }
};
var validationSchema = function (obj) {
    return new parameters(obj);
};
exports.validationSchema = validationSchema;
var string = function (options) {
    return new StringSchema(options);
};
exports.string = string;
var number = function (options) {
    return new NumberSchema(options);
};
exports.number = number;
var processRequest = function (SchemaObject, validationObject, method) {
    // console.log(JSON.stringify(validationObject));
    // console.log(JSON.stringify(SchemaObject));
    // console.log(Object.keys(SchemaObject.fields).length);
    // console.log(Object.keys(validationObject).length);
    if (Object.keys(SchemaObject.fields).length >= Object.keys(validationObject).length) {
        var _obj = {};
        var SchemaObjectFields = SchemaObject.fields;
        for (var property in SchemaObjectFields) {
            // console.log('property',property);
            // console.log('SchemaObjectFields[property]',SchemaObjectFields[property]);
            if (SchemaObjectFields[property].requestMethod[method]) {
                if (validationObject[property]) {
                    // if (typeof validationObject[property] === SchemaObjectFields[property].type) {
                    if (SchemaObjectFields[property]._typeCheck(validationObject[property])) {
                        // console.log('SchemaObjectFields[property].tests',SchemaObjectFields[property].tests);
                        checkForRequiredParam(SchemaObjectFields, property, validationObject, _obj);
                    }
                    else {
                        _obj["".concat(property)] = SchemaObjectFields[property].typeMessage.replace('${path}', property);
                    }
                    // }
                }
                else {
                    _obj["".concat(property)] = "".concat(property, " field is mandatory");
                }
            }
            else {
                if (validationObject[property]) {
                    if (SchemaObjectFields[property]._typeCheck(validationObject[property])) {
                        // console.log('SchemaObjectFields[property].tests',SchemaObjectFields[property].tests);
                        checkForRequiredParam(SchemaObjectFields, property, validationObject, _obj);
                    }
                    else {
                        _obj["".concat(property)] = SchemaObjectFields[property].typeMessage.replace('${path}', property);
                    }
                }
            }
        }
        // console.log(JSON.stringify(_obj));
        return _obj;
    }
    else {
        throw {
            message: 'No of keys In Schema Object and valodation object must be same',
        };
    }
};
exports.processRequest = processRequest;
