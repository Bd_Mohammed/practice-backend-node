"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mongoError = exports.insufficientParameters = exports.failureResponse = exports.successResponse = void 0;
var modules_service_model_1 = require("./modules.service.model");
function successResponse(message, DATA, res, logger) {
    logger.info("[Response] status code : ".concat(modules_service_model_1.response_status_codes.success), {
        STATUS: 'SUCCESS',
        MESSAGE: message,
        DATA: DATA
    });
    res.status(modules_service_model_1.response_status_codes.success).json({
        STATUS: 'SUCCESS',
        MESSAGE: message,
        DATA: DATA
    });
}
exports.successResponse = successResponse;
function failureResponse(message, DATA, res, logger) {
    logger.error("[Response] status code : ".concat(modules_service_model_1.response_status_codes.bad_request), {
        STATUS: 'FAILURE',
        MESSAGE: message,
        DATA: DATA
    });
    res.status(modules_service_model_1.response_status_codes.bad_request).json({
        STATUS: 'FAILURE',
        MESSAGE: message,
        DATA: DATA
    });
}
exports.failureResponse = failureResponse;
function insufficientParameters(res, data, logger) {
    logger.info("[Response] status code : ".concat(modules_service_model_1.response_status_codes.bad_request), {
        STATUS: 'FAILURE',
        MESSAGE: 'Insufficient parameters',
        DATA: data,
    });
    res.status(modules_service_model_1.response_status_codes.bad_request).json({
        STATUS: 'FAILURE',
        MESSAGE: 'Insufficient parameters',
        DATA: data,
    });
}
exports.insufficientParameters = insufficientParameters;
function mongoError(err, res, logger) {
    logger.error("[Response] status code : ".concat(modules_service_model_1.response_status_codes.internal_server_error), {
        STATUS: 'FAILURE',
        MESSAGE: 'MongoDB error',
        DATA: err
    });
    res.status(modules_service_model_1.response_status_codes.internal_server_error).json({
        STATUS: 'FAILURE',
        MESSAGE: 'MongoDB error',
        DATA: err
    });
}
exports.mongoError = mongoError;
