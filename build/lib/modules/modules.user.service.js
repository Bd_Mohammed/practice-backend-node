"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var modules_user_model_1 = __importDefault(require("./modules.user.model"));
var UserAuthService = /** @class */ (function () {
    function UserAuthService() {
    }
    UserAuthService.prototype.createUser = function (user_params, callback) {
        var _session = new modules_user_model_1.default(user_params);
        _session.save(callback);
    };
    UserAuthService.prototype.filterUser = function (query, callback) {
        modules_user_model_1.default.findOne(query, callback);
    };
    UserAuthService.prototype.updateUser = function (user_params, callback) {
        var query = { _id: user_params._id };
        modules_user_model_1.default.findOneAndUpdate(query, user_params, callback);
    };
    UserAuthService.prototype.deleteUser = function (_id, callback) {
        var query = { _id: _id };
        modules_user_model_1.default.deleteOne(query, callback);
    };
    return UserAuthService;
}());
exports.default = UserAuthService;
