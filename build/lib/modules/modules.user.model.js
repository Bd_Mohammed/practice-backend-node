"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var modules_service_model_1 = require("./modules.service.model");
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var schema = new Schema({
    name: String,
    email: String,
    role: String,
    password: String,
    is_deleted: {
        type: Boolean,
        default: false
    },
    modification_notes: [modules_service_model_1.ModificationNote]
});
exports.default = mongoose_1.default.model('users', schema);
