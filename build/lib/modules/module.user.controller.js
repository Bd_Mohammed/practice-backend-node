"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAuthController = void 0;
var modules_common_model_1 = require("./modules.common.model");
var modules_user_service_1 = __importDefault(require("./modules.user.service"));
var jwt_utils_1 = require("../Jwt/jwt.utils");
var customYup_1 = require("../customYup/customYup");
var UserAuthController = /** @class */ (function () {
    function UserAuthController() {
        this.user_auth_service = new modules_user_service_1.default();
    }
    UserAuthController.prototype.create_user = function (req, res, logger, user_custom_yup) {
        var _this = this;
        // this check whether all the fields were send through the request or not
        logger.info('[Request]: create_user param', req.body);
        try {
            var processReq = (0, customYup_1.processRequest)(user_custom_yup, req.body, 'post');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                (0, modules_common_model_1.insufficientParameters)(res, processReq, logger);
                return;
            }
        }
        catch (error) {
            logger.error("[ERROR]: create_user process request", error);
            (0, modules_common_model_1.failureResponse)('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }
        var user_params = {
            name: req.body.name.trim(),
            email: req.body.email.trim(),
            role: req.body.role,
            password: req.body.password.trim(),
            modification_notes: [{
                    modified_on: new Date(Date.now()),
                    modified_by: null,
                    modification_note: 'New user created'
                }],
        };
        var user_filter = { email: req.body.email };
        this.user_auth_service.filterUser(user_filter, function (err, user_data) {
            if (err) {
                (0, modules_common_model_1.mongoError)(err, res, logger);
            }
            else {
                if (!user_data) {
                    _this.user_auth_service.createUser(user_params, function (err, user_data) {
                        if (err) {
                            (0, modules_common_model_1.mongoError)(err, res, logger);
                        }
                        else {
                            try {
                                var response = __assign({ token: (0, jwt_utils_1.generateToken)(user_params) }, user_data._doc);
                                (0, modules_common_model_1.successResponse)('create user successful', response, res, logger);
                            }
                            catch (error) {
                                logger.error('[Error] in sending response : ', error);
                                (0, modules_common_model_1.failureResponse)('Internal Server Error', {
                                    message: 'Internal Server Error'
                                }, res, logger);
                            }
                        }
                    });
                }
                else {
                    (0, modules_common_model_1.failureResponse)('User Already Exists', {
                        message: 'User Already Exists'
                    }, res, logger);
                }
            }
        });
    };
    UserAuthController.prototype.validate_user = function (req, res, logger, user_custom_yup) {
        logger.info('[Request]: get_user param', req.params);
        try {
            var processReq = (0, customYup_1.processRequest)(user_custom_yup, req.body, 'get');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                (0, modules_common_model_1.insufficientParameters)(res, processReq, logger);
                return;
            }
        }
        catch (error) {
            logger.error("[ERROR]: get_user process request", error);
            (0, modules_common_model_1.failureResponse)('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }
        var user_filter = { email: req.body.email };
        this.user_auth_service.filterUser(user_filter, function (err, user_data) {
            if (err) {
                (0, modules_common_model_1.mongoError)(err, res, logger);
            }
            else {
                if (user_data) {
                    if (user_data.password === req.body.password) {
                        try {
                            var response = __assign({ token: (0, jwt_utils_1.generateToken)(user_data) }, user_data._doc);
                            (0, modules_common_model_1.successResponse)('GET User Successful', response, res, logger);
                        }
                        catch (error) {
                            logger.error('[Error] in sending response : ', error);
                            (0, modules_common_model_1.failureResponse)('Internal Server Error', {
                                message: 'Internal Server Error'
                            }, res, logger);
                        }
                    }
                    else {
                        (0, modules_common_model_1.failureResponse)('Incorrect password', {
                            message: 'Incorrect password'
                        }, res, logger);
                    }
                }
                else {
                    (0, modules_common_model_1.failureResponse)('User Not Found', {
                        message: 'User Not Found'
                    }, res, logger);
                }
            }
        });
    };
    UserAuthController.prototype.update_user = function (req, res, logger, user_custom_yup) {
        var _this = this;
        logger.info('[Request]: update_user param', {
            body: req.body,
            params: req.params
        });
        try {
            var processReq = (0, customYup_1.processRequest)(user_custom_yup, __assign({ id: req.params.id }, req.body), 'update');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                (0, modules_common_model_1.insufficientParameters)(res, processReq, logger);
                return;
            }
        }
        catch (error) {
            logger.error("[ERROR]: update_user process request", error);
            (0, modules_common_model_1.failureResponse)('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }
        var user_filter = { _id: req.params.id };
        this.user_auth_service.filterUser(user_filter, function (err, user_data) {
            if (err) {
                (0, modules_common_model_1.mongoError)(err, res, logger);
            }
            else if (user_data) {
                user_data.modification_notes.push({
                    modified_on: new Date(Date.now()),
                    modified_by: null,
                    modification_note: 'User data updated'
                });
                var user_params = {
                    _id: req.params.id,
                    name: req.body.name ? req.body.name : user_data.name,
                    email: req.body.email ? req.body.email : user_data.email,
                    role: req.body.role ? req.body.role : user_data.role,
                    password: req.body.password ? req.body.password : user_data.password,
                    is_deleted: req.body.is_deleted ? req.body.is_deleted : user_data.is_deleted,
                    modification_notes: user_data.modification_notes
                };
                _this.user_auth_service.updateUser(user_params, function (err, user_data) {
                    if (err) {
                        (0, modules_common_model_1.mongoError)(err, res, logger);
                    }
                    else {
                        (0, modules_common_model_1.successResponse)('update user successful', user_data, res, logger);
                    }
                });
            }
            else {
                (0, modules_common_model_1.failureResponse)('Invalid User', {
                    message: 'Invalid User'
                }, res, logger);
            }
        });
    };
    UserAuthController.prototype.delete_user = function (req, res, logger, user_custom_yup) {
        logger.info('[Request]: delete_user param', req.params);
        try {
            var processReq = (0, customYup_1.processRequest)(user_custom_yup, req.params, 'delete');
            if (processReq // 👈 null and undefined check
                && Object.keys(processReq).length !== 0
                && Object.getPrototypeOf(processReq) === Object.prototype) {
                (0, modules_common_model_1.insufficientParameters)(res, processReq, logger);
                return;
            }
        }
        catch (error) {
            logger.error("[ERROR]: delete_user process request", error);
            (0, modules_common_model_1.failureResponse)('Internal Server Error', {
                message: 'Internal Server Error'
            }, res, logger);
            return;
        }
        // if (req.params.id) {
        this.user_auth_service.deleteUser(req.params.id, function (err, delete_details) {
            if (err) {
                (0, modules_common_model_1.mongoError)(err, res, logger);
            }
            else if (delete_details.deletedCount !== 0) {
                (0, modules_common_model_1.successResponse)('User Deleted Successful', {
                    message: 'User Deleted Successful'
                }, res, logger);
            }
            else {
                (0, modules_common_model_1.failureResponse)('Invalid User', {
                    message: 'Invalid User'
                }, res, logger);
            }
        });
    };
    return UserAuthController;
}());
exports.UserAuthController = UserAuthController;
